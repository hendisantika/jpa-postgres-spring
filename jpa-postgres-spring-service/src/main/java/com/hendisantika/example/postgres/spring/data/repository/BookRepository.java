package com.hendisantika.example.postgres.spring.data.repository;

import com.hendisantika.example.postgres.spring.data.entity.BookEntity;
import com.hendisantika.example.postgres.spring.model.Genre;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-postgres-spring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.56
 * To change this template use File | Settings | File Templates.
 */
public interface BookRepository extends CrudRepository<BookEntity, UUID> {

    List<BookEntity> findByTitleIgnoreCase(String title);

    List<BookEntity> findByTitleIgnoreCaseAndAuthorIgnoreCase(String title,
            String author);

    List<BookEntity> findByTitleIgnoreCaseAndAuthorIgnoreCaseAndGenre(
            String title,
            String author,
            Genre genre);

    List<BookEntity> findByTitleIgnoreCaseAndGenre(String title, Genre genre);

    List<BookEntity> findByAuthorIgnoreCase(String author);

    List<BookEntity> findByAuthorIgnoreCaseAndGenre(String author, Genre genre);

    List<BookEntity> findByGenre(Genre genre);
}
