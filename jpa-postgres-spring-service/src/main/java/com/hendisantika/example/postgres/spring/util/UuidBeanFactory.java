package com.hendisantika.example.postgres.spring.util;

import org.dozer.BeanFactory;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-postgres-spring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 06.03
 * To change this template use File | Settings | File Templates.
 */
public class UuidBeanFactory implements BeanFactory {
    @Override
    public Object createBean(Object source, Class<?> sourceClass,
            String targetBeanId) {
        if (source == null) {
            return null;
        }

        UUID uuidSrc = (UUID) source;
        UUID uuidDest = new UUID(uuidSrc.getMostSignificantBits(),
                uuidSrc.getLeastSignificantBits());
        return uuidDest;
    }
}
