package com.hendisantika.example.postgres.spring.data.entity;

import com.hendisantika.example.postgres.spring.model.Genre;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-postgres-spring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.51
 * To change this template use File | Settings | File Templates.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "books", schema = "example")
public class BookEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
//    @GeneratedValue(generator = "uuid-gen")
//    @Type(type = "pg-uuid")

    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Type(  = "crdb-uuid", parameters = @Parameter(name = "column", value = "id"))

//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
//    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")

    private UUID id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "genre", nullable = false)
//    @Type(type = "com.hendisantika.example.postgres.spring.data.usertype.PGEnumUserType",
//            parameters = {@Parameter(name = "enumClassName",
//                    value = "com.hendisantika.example.postgres.spring.model.Genre")})
    private Genre genre;
}
