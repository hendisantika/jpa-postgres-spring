package com.hendisantika.example.postgres.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-postgres-spring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.47
 * To change this template use File | Settings | File Templates.
 */
@SpringBootApplication
@ComponentScan(basePackages = {
        "com.hendisantika"})
@EntityScan(basePackages = "com.hendisantika.example.postgres.spring.data.entity")
@EnableJpaRepositories(basePackages = {"com.hendisantika.example.postgres.spring.data.repository"})
public class BookApplication {
    public static void main(String[] args) {
        SpringApplication.run(BookApplication.class, args);
    }
}
