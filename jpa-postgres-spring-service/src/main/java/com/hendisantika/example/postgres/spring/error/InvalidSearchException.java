package com.hendisantika.example.postgres.spring.error;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@code InvalidSearchException} is a runtime exception if invalid search
 * criteria is entered.
 * <p/>
 *
 * Created by IntelliJ IDEA.
 * Project : jpa-postgres-spring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/10/17
 * Time: 05.39
 * To change this template use File | Settings | File Templates.
 */

@NoArgsConstructor
@ToString(callSuper = true)
@Getter
@Setter
public class InvalidSearchException extends RuntimeException {
    public InvalidSearchException(String s) {
        super(s);
    }
}
