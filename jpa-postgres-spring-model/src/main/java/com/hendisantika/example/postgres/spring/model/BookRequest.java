package com.hendisantika.example.postgres.spring.model;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-postgres-spring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.38
 * To change this template use File | Settings | File Templates.
 */
@Data
public class BookRequest {
    private String title;
    private String author;
    private Genre genre;
}
