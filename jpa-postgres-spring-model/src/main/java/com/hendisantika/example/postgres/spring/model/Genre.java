package com.hendisantika.example.postgres.spring.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-postgres-spring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.41
 * To change this template use File | Settings | File Templates.
 */
public enum Genre {

    DRAMA, ROMANCE, GUIDE, TRAVEL;

    /**
     * Returns a <tt>Genre<tt> enum based on string matching
     *
     * @param value string stored in database
     * @return a matching <tt>Genre</tt>
     */
    @JsonCreator
    public static Genre fromValue(String value) {
        return valueOf(value.toUpperCase());
    }

    /**
     * Converts a <tt>Genre</tt> to matching type string
     *
     * @param genre service enum
     * @return matching type string
     */
    @JsonValue
    public static String toValue(Genre genre) {
        return genre.name().toLowerCase();
    }
}
